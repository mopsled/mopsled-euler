#include <stdio.h> 
#include <stdint.h>
#include "prime-tools.h"

int main() {
	uint64_t magicNumber = 600851475143ULL;

	PrimeNode *list = NULL;
	primeFactors(magicNumber, &list);

	PrimeNode *cursor = list;
	while (cursor->next) {
		cursor = cursor->next;
	}

	printf("Largest prime factor: %d\n", cursor->prime);
}