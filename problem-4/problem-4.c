#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

int digitCount(int number) {
	if (number == 0) {
		return 1;
	}

	int length = 0;
	while (number != 0) {
		number /= 10;
		++length;
	}

	return length;
}

void testDigitCount() {
	assert(digitCount(0) == 1);
	assert(digitCount(1) == 1);
	assert(digitCount(10) == 2);
	assert(digitCount(15) == 2);
	assert(digitCount(100) == 3);
	assert(digitCount(4321) == 4);
	assert(digitCount(1234123) == 7);
}

int getDigit(int number, int digit) {
	if (number < 0 || digit < 0) {
		return -1;
	} else if (number == 0) {
		return 0;
	}

	int numberLength = digitCount(number);

	if (digit >= numberLength) {
		return -1;
	}

	int *digits = calloc(numberLength, sizeof(int));

	for (int i = 0; number; ++i) {
		digits[i] = number % 10;
		number /= 10;
	}

	int returnDigit = digits[numberLength - digit - 1];
	free(digits);

	return returnDigit;
}

void testGetDigit() {
	assert(getDigit(0, 0) == 0);
	assert(getDigit(1, 0) == 1);
	assert(getDigit(42, 0) == 4);
	assert(getDigit(163, 2) == 3);
	assert(getDigit(99699, 2) == 6);
	assert(getDigit(12, -1) == -1);
	assert(getDigit(-12, -1) == -1);
	assert(getDigit(-12, 1) == -1);
	assert(getDigit(1, 1) == -1);
	assert(getDigit(1, 2) == -1);
}

int isPalindrome(int number) {
	if (number < 0) {
		return 0;
	}

	int length = digitCount(number);
	if (length == 1) {
		return 1;
	}

	int startIndex = 0, endIndex = length - 1;

	while (startIndex < endIndex) {
		if (getDigit(number, startIndex) != getDigit(number, endIndex)) {
			return 0;
		}

		++startIndex;
		--endIndex;
	}

	return 1;
}

void testIsPalindrome() {
	int palindromes[] = {0, 1, 11, 66, 202, 242, 9009, 12321, 2554552};
	int palindromesSize = sizeof(palindromes)/sizeof(int);
	for (int i = 0; i < palindromesSize; ++i) {
		assert(isPalindrome(palindromes[i]));
	}

	int nonPalindromes[] = {12, 20, 100, 9909, 123123};
	int nonPalindromesSize = sizeof(nonPalindromes)/sizeof(int);
	for (int i = 0; i < nonPalindromesSize; ++i) {
		assert(!isPalindrome(nonPalindromes[i]));
	}

	int negativeInputs[] = {-100, -1};
	int negativeInputsSize = sizeof(negativeInputs)/sizeof(int);
	for (int i = 0; i < negativeInputsSize; ++i) {
		assert(!isPalindrome(negativeInputs[i]));
	}
}

int main() {
	int maxPalindrome = 0;

	for (int i = 99; i < 1000; ++i) {
		for (int j = 99; j < 1000; ++j) {
			int product = i * j;
			if (isPalindrome(product)) {
				if (product > maxPalindrome) {
					maxPalindrome = product;
				}
			}
		}
	}

	printf("%d\n", maxPalindrome);
}