typedef struct PrimeNode PrimeNode;
struct PrimeNode {
	int prime;
	PrimeNode *next;
};

int isPrime(int number);

void primeFactors(long long number, PrimeNode **list);