#include <math.h>
#include <stdlib.h>
#include "prime-tools.h"

int isPrime(int number) {
	if (number <= 1)
		return 0;
	if (number == 2) 
		return 1;
	else if (!(number % 2))
		return 0;

	int upperLimit = sqrt(number);
	for (int i = 3; i <= upperLimit; i += 2) {
		if (!(number % i)) return 0;
	}

	return 1;
}

void primeFactors(long long number, PrimeNode **list) {
	int i = 2;

	while (i <= number) {
		if ((number % i == 0) && isPrime(i)) {
			PrimeNode *nextNode = calloc(1, sizeof(PrimeNode));
			nextNode->prime = i;

			if (*list == NULL) {
				*list = nextNode;
			} else {
				(*list)->next = nextNode;
			}

			list = &((*list)->next);
			number = number / i;
			i = 2;
		} else {
			i += 1;
		}
	}
}