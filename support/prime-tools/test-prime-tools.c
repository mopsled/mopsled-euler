#include <stdio.h>
#include <assert.h>

#include "prime-tools.h"

void testIsPrime() {
	int primes[] = {2, 3, 5, 11, 41, 43, 997};
	int primesSize = sizeof(primes)/sizeof(int);
	for (int i = 0; i < primesSize; ++i) {
		assert( isPrime(primes[i]) );
	}

	int nonPrimes[] = {-100, -2, 0, 1, 4, 8, 32, 42, 100, 99999};
	int nonPrimesSize = sizeof(nonPrimes)/sizeof(int);
	for (int i = 0; i < nonPrimesSize; ++i) {
		assert( !isPrime(nonPrimes[i]) );
	}
}

// testPrimeFactors helper method
void testPrimeListWithArray(PrimeNode *list, int array[], int size) {
	PrimeNode *cursor = list;
	for (int i = 0; i < size; i++) {
		assert(cursor != NULL);
		assert(cursor->prime == array[i]);
		cursor = cursor->next;
	}

	assert(cursor == NULL);
}

// testPrimeFactors helper method
void freePrimeList(PrimeNode *list) {
	PrimeNode *nodeToFree, *cursor = list;
	while (cursor) {
		nodeToFree = cursor;
		cursor = cursor->next;
		free(nodeToFree);
	}
}

void testPrimeFactors() {
	PrimeNode *list = NULL;
	primeFactors(8, &list);
	int factorsOf8[] = {2, 2, 2};
	testPrimeListWithArray(list, factorsOf8, 3);
	freePrimeList(list);

	list = NULL;
	primeFactors(30, &list);
	int factorsOf30[] = {2, 3, 5};
	testPrimeListWithArray(list, factorsOf30, 3);
	freePrimeList(list);

	list = NULL;
	primeFactors(100, &list);
	int factorsOf100[] = {2, 2, 5, 5};
	testPrimeListWithArray(list, factorsOf100, 4);
	freePrimeList(list);

	list = NULL;
	primeFactors(3, &list);
	int factorsOf3[] = {3};
	testPrimeListWithArray(list, factorsOf3, 1);
	freePrimeList(list);

	list = NULL;
	primeFactors(0, &list);
	int factorsOf0[] = {};
	testPrimeListWithArray(list, factorsOf0, 0);
	freePrimeList(list);

	list = NULL;
	primeFactors(1, &list);
	int factorsOf1[] = {};
	testPrimeListWithArray(list, factorsOf1, 0);
	freePrimeList(list);

	list = NULL;
	primeFactors(100001, &list);
	int factorsOf100001[] = {11, 9091};
	testPrimeListWithArray(list, factorsOf100001, 2);
	freePrimeList(list);
}

int main() {
	testIsPrime();
	testPrimeFactors();
	printf("Tests passed!\n");
}