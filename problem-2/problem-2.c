#include <stdio.h>

int main() {
	int sum = 0;
	int fibNext, fibCurrent = 2, fibLast = 1;

	while (fibCurrent <= 4000000) {
		if (!(fibCurrent % 2)) {
			sum += fibCurrent;
		}

		fibNext = fibCurrent + fibLast;
		fibLast = fibCurrent;
		fibCurrent = fibNext;
	}

	printf("sum: %d\n", sum);
}