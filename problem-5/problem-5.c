#include <stdio.h>

int main() {
	for(int number = 1; ; ++number) {
		int divisible = 1;
		for (int i = 1; i <= 20; ++i) {
			if (number % i) {
				divisible = 0;
				break;
			}
		}

		if (divisible) {
			printf("%d\n", number);
			break;
		}
	}
}